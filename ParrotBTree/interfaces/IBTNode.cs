namespace Parrot.BTree
{
    public interface IBTNode
    {
        Status CurrentState{get;}
        Status Run();
    }
}